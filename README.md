server-mgmt-console
==============

Firefox over Docker via VNC including the necessary Java plugins to support working with a bunch of old Java/Web based management interfaces, such as old Dell, IBM, Fujitsu, or Brocade FC switches. This is the product of not being able to access old Lenovo servers using current browsers.   
This Docker image provides an easy way to spin up a browser with full support.

This Dockerfile is based on the work found here: https://github.com/creack/docker-firefox

How to execute is desribed in branches with specific Java version.

